# -*- coding: utf-8 -*-
"""
Test vectors for JWE.

From:
https://hexdocs.pm/jose/JOSE.JWE.html

Linked source:
https://gist.github.com/potatosalad/dd140560b2bdbdab886d
"""

# Created: 2018-08-06 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.

JWK_OCT256 = (
    '{"k":"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"'
    ',"kty":"oct"}'
)
JWK_OCT256_DICT = {
    'k': 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
    'kty': 'oct'
}
JWK_X25519_ALICE_SK = {
    'crv': 'X25519',
    'd': '9GIWie9Ofte9inIuwdrJqpS2G5znm1kWDna29dapshY',
    'kty': 'OKP',
    'x': 'gH7N2pOGnzv_wKA-HjDFJNUReX_tm9_7b2FRR27qqXs'
}
JWK_X25519_BOB_SK = {
    'crv': 'X25519',
    'd': 'qqd68WxDa2EIUgZB-6JSwPSD4D0EdgMPxMVNGNiO4SI',
    'kty': 'OKP',
    'x': 'cWmFC9fB6J9PEwHutgfZWMk82r-CgYyekM5plrBHjBA'
}
# This one is should fail, as it is using an algorithm value not compatible
# with the latest draft using `C20P` (but it's using `ChaCha20/Poly1305`).
# from https://hexdocs.pm/jose/JOSE.JWE.html
JWE_CHACHA20POLY1305 = (
    'eyJhbGciOiJkaXIiLCJlbmMiOiJDMjBQIn0.'
    '.'
    'gunc-Xr1t1jqZX1l.'
    '8Yc.'
    'yi9qKB4ANjfQCPjgYwf-zQ'
)
JWE_CHACHA20POLY1305_DICT = {
    'message': {},
    'header': b'{"alg":"dir","enc":"C20P"}',
    'key': None,
    'nonce': b'\x82\xe9\xdc\xf9z\xf5\xb7X\xeae}e',
    'ciphertext': b'\xf1\x87',
    'tag': b'K\x10\xb0v\x05\xff\x9a\xeb]w\x8b\xfb&J*\x0e'
}

# from https://hexdocs.pm/jose/JOSE.JWE.html
JWE_AES256GCM = (
    'eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0.'
    '.'
    'rDTJhd5ja5pDAYtn.'
    'PrM.'
    'MQdLgiVXQsG_cLas93ZEHw'
)
JWE_AES256GCM_DICT = {
    'message': {},
    'header': b'{"alg":"dir","enc":"A256GCM"}',
    'key': None,
    'nonce': b'\xac4\xc9\x85\xdeck\x9aC\x01\x8bg',
    'ciphertext': b'>\xb3',
    'tag': b'1\x07K\x82%WB\xc1\xbfp\xb6\xac\xf7vD\x1f'
}

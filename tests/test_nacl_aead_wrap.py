# -*- coding: utf-8 -*-
"""Tests for the libsodium wrapper module."""

# Created: 2018-07-30 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

from binascii import unhexlify
from unittest import mock  # @UnusedImport # noqa: F401
import unittest

from sspyjose.nacl_aead_wrap import (chacha20poly1305_encrypt,
                                     chacha20poly1305_decrypt)
from tests.rfc7539_test_data import TEST_VECTORS


class SodiumWrapTest(unittest.TestCase):
    """Testing the libsodium wrapper module."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_ietf_jwe_test_encrypt_vector(self):
        """Encrypt using the vectors."""
        for test in TEST_VECTORS:
            key = unhexlify(test['key'])
            nonce = unhexlify(test['nonce'])
            message = unhexlify(test['message'])
            aad = unhexlify(test['aad'])
            ciphertext = unhexlify(test['ciphertext'])
            tag = unhexlify(test['tag'])
            result = chacha20poly1305_encrypt(message, aad, nonce, key)
            self.assertTupleEqual(result, (ciphertext, tag))

    def test_ietf_jwe_test_decrypt_vector(self):
        """Decrypt using the vectors."""
        for test in TEST_VECTORS:
            key = unhexlify(test['key'])
            nonce = unhexlify(test['nonce'])
            message = unhexlify(test['message'])
            aad = unhexlify(test['aad'])
            ciphertext = unhexlify(test['ciphertext'])
            tag = unhexlify(test['tag'])
            result = chacha20poly1305_decrypt(ciphertext, tag, aad, nonce, key)
            self.assertEqual(result, message)


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

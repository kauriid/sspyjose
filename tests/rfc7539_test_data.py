# -*- coding: utf-8 -*-
"""
AEAD test vectors for ChaCha20/Poly1305 from RFC7539.
"""

# Created: 2018-07-30 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

TEST_VECTORS = [
    {
        # Test vector from Section 2.8.2
        'key': ('808182838485868788898a8b8c8d8e8f909192939495969798999a9b'
                '9c9d9e9f'),
        'nonce': '070000004041424344454647',
        'message': ('4c616469657320616e642047656e746c656d656e206f66207468'
                    '6520636c617373206f66202739393a204966204920636f756c64'
                    '206f6666657220796f75206f6e6c79206f6e652074697020666f'
                    '7220746865206675747572652c2073756e73637265656e20776f'
                    '756c642062652069742e'),
        'aad': '50515253c0c1c2c3c4c5c6c7',
        'ciphertext': ('d31a8d34648e60db7b86afbc53ef7ec2a4aded51296e08fea'
                       '9e2b5a736ee62d63dbea45e8ca9671282fafb69da92728b1a'
                       '71de0a9e060b2905d6a5b67ecd3b3692ddbd7f2d778b8c980'
                       '3aee328091b58fab324e4fad675945585808b4831d7bc3ff4'
                       'def08e4b7a9de576d26586cec64b6116'),
        'tag': '1ae10b594f09e26a7e902ecbd0600691'
    },
    {
        # Test vector from RFC7539 Appendix A.5
        'key': ('1c9240a5eb55d38af333888604f6b5f0473917c1402b80099dca5cbc'
                '207075c0'),
        'nonce': '000000000102030405060708',
        'message': ('496e7465726e65742d4472616674732061726520647261667420'
                    '646f63756d656e74732076616c696420666f722061206d617869'
                    '6d756d206f6620736978206d6f6e74687320616e64206d617920'
                    '626520757064617465642c207265706c616365642c206f72206f'
                    '62736f6c65746564206279206f7468657220646f63756d656e74'
                    '7320617420616e792074696d652e20497420697320696e617070'
                    '726f70726961746520746f2075736520496e7465726e65742d44'
                    '7261667473206173207265666572656e6365206d617465726961'
                    '6c206f7220746f2063697465207468656d206f74686572207468'
                    '616e206173202fe2809c776f726b20696e2070726f6772657373'
                    '2e2fe2809d'),
        'aad': 'f33388860000000000004e91',
        'ciphertext': ('64a0861575861af460f062c79be643bd5e805cfd345cf389f'
                       '108670ac76c8cb24c6cfc18755d43eea09ee94e382d26b0bd'
                       'b7b73c321b0100d4f03b7f355894cf332f830e710b97ce98c'
                       '8a84abd0b948114ad176e008d33bd60f982b1ff37c8559797'
                       'a06ef4f0ef61c186324e2b3506383606907b6a7c02b0f9f61'
                       '57b53c867e4b9166c767b804d46a59b5216cde7a4e99040c5'
                       'a40433225ee282a1b0a06c523eaf4534d7f83fa1155b00477'
                       '18cbc546a0d072b04b3564eea1b422273f548271a0bb23160'
                       '53fa76991955ebd63159434ecebb4e466dae5a1073a672762'
                       '7097a1049e617d91d361094fa68f0ff77987130305beaba2e'
                       'da04df997b714d6c6f2c29a6ad5cb4022b02709b'),
        'tag': 'eead9d67890cbb22392336fea1851f38'
    },
    {
        # Test vector from RFC7634 Appendix A
        'key': ('808182838485868788898a8b8c8d8e8f909192939495969798999a9b'
                '9c9d9e9f'),
        'nonce': 'a0a1a2a31011121314151617',
        'message': ('45000054a6f200004001e778c6336405c000020508005b7a3a08'
                    '0000553bec100007362708090a0b0c0d0e0f1011121314151617'
                    '18191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f3031'
                    '32333435363701020204'),
        'aad': '0102030400000005',
        'ciphertext': ('24039428b97f417e3c13753a4f05087b67c352e6a7fab1b98'
                       '2d466ef407ae5c614ee8099d52844eb61aa95dfab4c02f72a'
                       'a71e7c4c4f64c9befe2facc638e8f3cbec163fac469b50277'
                       '3f6fb94e664da9165b82829f641e0'),
        'tag': '76aaa8266b7fb0f7b11b369907e1ad43'
    },
    {
        # Test vector from RFC7634 Appendix B
        'key': ('808182838485868788898a8b8c8d8e8f909192939495969798999a9b'
                '9c9d9e9f'),
        'nonce': 'a0a1a2a31011121314151617',
        'message': '0000000c000040010000000a00',
        'aad': ('c0c1c2c3c4c5c6c7d0d1d2d3d4d5d6d72e2025000000000900000045'
                '29000029'),
        'ciphertext': '610394701f8d017f7c12924889',
        'tag': '6b71bfe25236efd7cdc67066906315b2'
    }
]

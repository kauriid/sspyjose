# -*- coding: utf-8 -*-
"""Tests for the jwk module."""

# Created: 2018-07-25 Guy K. Kloss <guy@mysinglesource.io>
#
# (c) 2018-2019 by SingleSource Limited, Auckland, New Zealand
#     http://mysinglesource.io/
#     Apache 2.0 Licence.
#
# This work is licensed under the Apache 2.0 open source licence.
# Terms and conditions apply.
#
# You should have received a copy of the licence along with this
# program.

__author__ = 'Guy K. Kloss <guy@mysinglesource.io>'

import base64
import json
from unittest import mock  # @UnusedImport
import unittest

import nacl.public
import nacl.signing

from sspyjose import (Jose,
                      utils)
from sspyjose.jwk import (Jwk,
                          Ed25519Jwk,
                          X25519Jwk,
                          ChaCha20Poly1305Jwk,
                          AES256GCMJwk)
from tests.data import (JWK_ARTHUR_FULL_ED25519,
                        JWK_ARTHUR_FULL_ED25519_DICT,
                        JWK_ARTHUR_PUB_ED25519,
                        JWK_ARTHUR_FULL_X25519,
                        JWK_ARTHUR_FULL_X25519_DICT,
                        JWK_ARTHUR_PUB_X25519,
                        JWK_CHACHA20POLY1305,
                        JWK_CHACHA20POLY1305_DICT,
                        JWK_AES256GCM,
                        JWK_AES256GCM_DICT,
                        JWE_X25519CHACHA20POLY1305_DECODED)


class JwkTest(unittest.TestCase):
    """Testing the Jwk class."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_get_instance_defaults(self):
        """Use defaults-only factory."""
        key = Jwk.get_instance()
        self.assertIsInstance(key, ChaCha20Poly1305Jwk)
        self.assertEqual(key._DEFAULT_HEADER['alg'], Jose.DEFAULT_ENC)

    def test_get_instance_specific_keys(self):
        """Use factory for specific keys."""
        tests = [('alg', 'C20P', ChaCha20Poly1305Jwk),
                 ('alg', 'A256GCM', AES256GCMJwk),
                 ('crv', 'Ed25519', Ed25519Jwk),
                 ('crv', 'X25519', X25519Jwk)]
        for kwkey, kwarg, klass in tests:
            key = Jwk.get_instance(**{kwkey: kwarg})
            self.assertIsInstance(key, klass)
            self.assertEqual(key._DEFAULT_HEADER[kwkey], kwarg)

    def test_get_instance_unsupported_cipher(self):
        """Use factory for an unsupported cipher."""
        self.assertRaises(RuntimeError, Jwk.get_instance,
                          **dict(alg='AES128GCM'))

    def test_get_instance_from_dict(self):
        """Use factory init with a key dict."""
        tests = [(JWK_CHACHA20POLY1305_DICT, 'alg', 'C20P',
                  ChaCha20Poly1305Jwk),
                 (JWK_AES256GCM_DICT, 'alg', 'A256GCM',
                  AES256GCMJwk),
                 (JWK_ARTHUR_FULL_ED25519_DICT, 'crv', 'Ed25519',
                  Ed25519Jwk),
                 (JWK_ARTHUR_FULL_X25519_DICT, 'crv', 'X25519',
                  X25519Jwk)]
        for key_dict, attrib, typecheck, klass in tests:
            key = Jwk.get_instance(from_dict=key_dict)
            self.assertIsInstance(key, klass)
            self.assertEqual(key._DEFAULT_HEADER[attrib], typecheck)

    def test_get_instance_from_json(self):
        """Use factory init with a key json."""
        tests = [(JWK_CHACHA20POLY1305, 'alg', 'C20P',
                  ChaCha20Poly1305Jwk),
                 (JWK_AES256GCM, 'alg', 'A256GCM',
                  AES256GCMJwk),
                 (JWK_ARTHUR_FULL_ED25519, 'crv', 'Ed25519',
                  Ed25519Jwk),
                 (JWK_ARTHUR_PUB_X25519, 'crv', 'X25519',
                  X25519Jwk)]
        for key_json, attrib, typecheck, klass in tests:
            key = Jwk.get_instance(from_json=key_json)
            self.assertIsInstance(key, klass)
            self.assertEqual(key._DEFAULT_HEADER[attrib], typecheck)

    def test_get_instance_from_secret(self):
        """Use factory init with a secret."""
        key = Jwk.get_instance(alg=JWK_AES256GCM_DICT['alg'],
                               from_secret=JWK_AES256GCM_DICT['k'])
        self.assertIsInstance(key, AES256GCMJwk)
        self.assertEqual(key.k, JWK_AES256GCM_DICT['k'])


class Ed25519JwkTest(unittest.TestCase):
    """Testing the Ed25519Jwk class."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla Ed25519 JWK key object."""
        result = Ed25519Jwk()
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'Ed25519')
        self.assertNotIn('d', result._data)
        self.assertNotIn('x', result._data)
        self.assertNotIn('kid', result._data)
        self.assertNotIn('_signing_key', result._data)
        self.assertNotIn('_verify_key', result._data)

    def test_constructor_with_json(self):
        """Make an Ed25519 JWK key from JSON data."""
        result = Ed25519Jwk(from_json=JWK_ARTHUR_FULL_ED25519)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'Ed25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_ED25519_DICT['d'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_ED25519_DICT['x'])
        self.assertEqual(result.kid, JWK_ARTHUR_FULL_ED25519_DICT['kid'])
        self.assertNotIn('_signing_key', result._data)
        self.assertNotIn('_verify_key', result._data)
        self.assertEqual(bytes(result._signing_key),
                         JWK_ARTHUR_FULL_ED25519_DICT['d'])

    def test_constructor_with_dict(self):
        """Make an Ed25519 JWK key from data in dict."""
        result = Ed25519Jwk(from_dict=JWK_ARTHUR_FULL_ED25519_DICT)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'Ed25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_ED25519_DICT['d'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_ED25519_DICT['x'])
        self.assertEqual(result.kid, JWK_ARTHUR_FULL_ED25519_DICT['kid'])
        self.assertNotIn('_signing_key', result._data)
        self.assertNotIn('_verify_key', result._data)
        self.assertEqual(bytes(result._signing_key),
                         JWK_ARTHUR_FULL_ED25519_DICT['d'])

    def test_constructor_with_dict_pub_string_key(self):
        """Make an Ed25519 JWK pub key from data in dict and a string key."""
        data = JWK_ARTHUR_FULL_ED25519_DICT.copy()
        del data['d']
        data['x'] = utils.bytes_to_string(data['x'])
        result = Ed25519Jwk(from_dict=data)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'Ed25519')
        self.assertNotIn('d', result._data)
        self.assertEqual(result.kid, JWK_ARTHUR_FULL_ED25519_DICT['kid'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_ED25519_DICT['x'])
        self.assertNotIn('_signing_key', result._data)
        self.assertEqual(bytes(result._verify_key),
                         JWK_ARTHUR_FULL_ED25519_DICT['x'])

    def test_constructor_with_dict_string_key(self):
        """Make an Ed25519 JWK full key from data in dict and a string key."""
        data = JWK_ARTHUR_FULL_ED25519_DICT.copy()
        data['x'] = utils.bytes_to_string(data['x'])
        data['d'] = utils.bytes_to_string(data['d'])
        result = Ed25519Jwk(from_dict=data)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'Ed25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_ED25519_DICT['d'])
        self.assertEqual(result.kid, JWK_ARTHUR_FULL_ED25519_DICT['kid'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_ED25519_DICT['x'])
        self.assertNotIn('_verify_key', result._data)
        self.assertEqual(bytes(result._signing_key),
                         JWK_ARTHUR_FULL_ED25519_DICT['d'])

    @mock.patch('nacl.signing.SigningKey.generate')
    def test_constructor_generate(self, generate_mock):
        """Generate an Ed25519 JWK key."""
        generate_mock.return_value = nacl.signing.SigningKey(
            JWK_ARTHUR_FULL_ED25519_DICT['d'])
        result = Ed25519Jwk(generate=True)
        self.assertEqual(generate_mock.call_count, 1)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'Ed25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_ED25519_DICT['d'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_ED25519_DICT['x'])
        self.assertNotIn('kid', result._data)
        self.assertEqual(bytes(result._signing_key),
                         JWK_ARTHUR_FULL_ED25519_DICT['d'])
        self.assertEqual(bytes(result._verify_key),
                         JWK_ARTHUR_FULL_ED25519_DICT['x'])

    def test_constructor_clash(self):
        """Calling constuctor with JSON and generate options."""
        self.assertRaises(ValueError, Ed25519Jwk,
                          from_json=JWK_ARTHUR_FULL_ED25519,
                          generate=True)

    def test_to_json_no_private(self):
        """Serialise an Ed25519 JWK to JSON without private elements."""
        key = Ed25519Jwk(from_json=JWK_ARTHUR_FULL_ED25519)
        result = key.to_json(exclude_private=True)
        result_dict = json.loads(result)
        compare = json.loads(JWK_ARTHUR_PUB_ED25519)
        self.assertDictEqual(result_dict, compare)

    def test_to_json_with_private(self):
        """Serialise an Ed25519 JWK to JSON with private elements."""
        key = Ed25519Jwk(from_json=JWK_ARTHUR_FULL_ED25519)
        result = key.to_json()
        result_dict = json.loads(result)
        compare = json.loads(JWK_ARTHUR_FULL_ED25519)
        self.assertDictEqual(result_dict, compare)

    def test_json_property(self):
        """Serialise an Ed25519 JWK to JSON via JSON property."""
        key = Ed25519Jwk(from_json=JWK_ARTHUR_FULL_ED25519)
        result = key.json
        compare = key.to_json()
        self.assertEqual(result, compare)

    def test_setting_key_property_as_bytes(self):
        """Setting key seed property with bytes."""
        key_value = b'pork' * 8
        key = Ed25519Jwk()
        key.d = key_value
        self.assertEqual(key.d, key_value)
        self.assertEqual(bytes(key._signing_key), key_value)
        self.assertEqual(base64.urlsafe_b64encode(key.x),
                         b'0EH0Dongc7DkKqLKwbJS5h19Q6ujxPltVfDqhzmWrNk=')

    def test_setting_key_property_as_str(self):
        """Setting key seed property with a str."""
        key_value = 'cG9ya3Bvcmtwb3JrcG9ya3Bvcmtwb3JrcG9ya3Bvcms'
        key = Ed25519Jwk()
        key.d = key_value
        self.assertEqual(key.d, 8 * b'pork')

    def test_setting_key_property_bad_key_size(self):
        """Setting key seed property with a mis-matched key size."""
        key = Ed25519Jwk()
        with self.assertRaises(ValueError):
            key.d = b"Don't Panic!"

    def test_setting_key_property_bad_key_type(self):
        """Setting key seed property with a mis-matched key type."""
        key = Ed25519Jwk()
        with self.assertRaises(ValueError):
            key.d = 42


class X25519JwkTest(unittest.TestCase):
    """Testing the X25519Jwk class."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla X25519 JWK key object."""
        result = X25519Jwk()
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'X25519')
        self.assertNotIn('d', result._data)
        self.assertNotIn('x', result._data)
        self.assertNotIn('kid', result._data)
        self.assertNotIn('_dh_key', result._data)

    def test_constructor_with_json(self):
        """Make an X25519 JWK key from JSON data."""
        result = X25519Jwk(from_json=JWK_ARTHUR_FULL_X25519)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'X25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_X25519_DICT['d'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_X25519_DICT['x'])
        self.assertEqual(result.kid, JWK_ARTHUR_FULL_X25519_DICT['kid'])
        self.assertNotIn('_dh_key', result._data)
        self.assertEqual(bytes(result._dh_key),
                         JWK_ARTHUR_FULL_X25519_DICT['d'])

    def test_constructor_with_dict(self):
        """Make an X25519 JWK key from data in dict."""
        result = X25519Jwk(from_dict=JWK_ARTHUR_FULL_X25519_DICT)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'X25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_X25519_DICT['d'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_X25519_DICT['x'])
        self.assertEqual(result.kid, JWK_ARTHUR_FULL_X25519_DICT['kid'])
        self.assertNotIn('_dh_key', result._data)
        self.assertEqual(bytes(result._dh_key),
                         JWK_ARTHUR_FULL_X25519_DICT['d'])

    def test_constructor_with_dict_pub_string_key(self):
        """Make an X25519 JWK pub key from data in dict and a string key."""
        result = X25519Jwk(
            from_dict=JWE_X25519CHACHA20POLY1305_DECODED['header']['epk'])
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'X25519')
        self.assertNotIn('d', result._data)
        self.assertNotIn('kid', result._data)
        self.assertEqual(result.x, JWK_ARTHUR_FULL_X25519_DICT['x'])
        self.assertNotIn('_dh_key', result._data)
        self.assertEqual(bytes(result._dh_key),
                         JWK_ARTHUR_FULL_X25519_DICT['x'])

    def test_constructor_with_dict_string_key(self):
        """Make an X25519 JWK full key from data in dict and a string key."""
        data = json.loads(JWK_ARTHUR_FULL_X25519)
        result = X25519Jwk(from_dict=data)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'X25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_X25519_DICT['d'])
        self.assertIn('kid', result._data)
        self.assertEqual(result.x, JWK_ARTHUR_FULL_X25519_DICT['x'])
        self.assertIn('_dh_key', result._data)
        self.assertEqual(bytes(result._dh_key),
                         JWK_ARTHUR_FULL_X25519_DICT['d'])

    @mock.patch('nacl.public.PrivateKey.generate')
    def test_constructor_generate(self, generate_mock):
        """Generate an X25519 JWK key."""
        generate_mock.return_value = nacl.public.PrivateKey(
            JWK_ARTHUR_FULL_X25519_DICT['d'])
        result = X25519Jwk(generate=True)
        self.assertEqual(generate_mock.call_count, 1)
        self.assertEqual(result.kty, 'OKP')
        self.assertEqual(result.crv, 'X25519')
        self.assertEqual(result.d, JWK_ARTHUR_FULL_X25519_DICT['d'])
        self.assertEqual(result.x, JWK_ARTHUR_FULL_X25519_DICT['x'])
        self.assertNotIn('kid', result._data)
        self.assertEqual(bytes(result._dh_key),
                         JWK_ARTHUR_FULL_X25519_DICT['d'])

    def test_constructor_clash(self):
        """Calling constuctor with JSON and generate options."""
        self.assertRaises(ValueError, X25519Jwk,
                          from_json=JWK_ARTHUR_FULL_X25519,
                          generate=True)

    def test_to_json_no_private(self):
        """Serialise an X25519 JWK to JSON without private elements."""
        key = X25519Jwk(from_json=JWK_ARTHUR_FULL_X25519)
        result = key.to_json(exclude_private=True)
        result_dict = json.loads(result)
        compare = json.loads(JWK_ARTHUR_PUB_X25519)
        self.assertDictEqual(result_dict, compare)

    def test_to_json_with_private(self):
        """Serialise an X25519 JWK to JSON with private elements."""
        key = X25519Jwk(from_json=JWK_ARTHUR_FULL_X25519)
        result = key.to_json()
        result_dict = json.loads(result)
        compare = json.loads(JWK_ARTHUR_FULL_X25519)
        self.assertDictEqual(result_dict, compare)

    def test_json_property(self):
        """Serialise an X25519 JWK to JSON via JSON property."""
        key = X25519Jwk(from_json=JWK_ARTHUR_FULL_X25519)
        result = key.json
        compare = key.to_json()
        self.assertEqual(result, compare)

    def test_setting_key_property_as_bytes(self):
        """Setting private key property with bytes."""
        key_value = b'pork' * 8
        key = X25519Jwk()
        key.d = key_value
        self.assertEqual(key.d, key_value)
        self.assertEqual(bytes(key._dh_key), key_value)
        self.assertEqual(base64.urlsafe_b64encode(key.x),
                         b'ZAGYgsl0srF6le1Ibib2we75_DDErh0P5dZCJ-mETVU=')

    def test_setting_key_property_as_str(self):
        """Setting private key property with a str."""
        key_value = 'cG9ya3Bvcmtwb3JrcG9ya3Bvcmtwb3JrcG9ya3Bvcms'
        key = X25519Jwk()
        key.d = key_value
        self.assertEqual(key.d, 8 * b'pork')

    def test_setting_key_property_bad_key_size(self):
        """Setting private key property with a mis-matched key size."""
        key = X25519Jwk()
        with self.assertRaises(ValueError):
            key.d = b"Don't Panic!"

    def test_setting_key_property_bad_key_type(self):
        """Setting private key property with a mis-matched key type."""
        key = X25519Jwk()
        with self.assertRaises(ValueError):
            key.d = 42

    def test_getting_dh_key_property_private_key(self):
        """Getting the _dh_key key property on a private key."""
        key = X25519Jwk(from_json=JWK_ARTHUR_FULL_X25519)
        self.assertIsInstance(key._dh_key, nacl.public.PrivateKey)

    def test_getting_dh_key_property_public_key(self):
        """Getting the _dh_key key property on a public key."""
        key = X25519Jwk(from_json=JWK_ARTHUR_PUB_X25519)
        self.assertIsInstance(key._dh_key, nacl.public.PublicKey)


class ChaCha20Poly1305JwkTest(unittest.TestCase):
    """Testing the ChaCha20Poly1305Jwk class."""

    def setUp(self):  # noqa: D102
        pass

    def tearDown(self):  # noqa: D102
        pass

    def test_vanilla_constructor(self):
        """Make a vanilla ChaCha20/Poly1305 JWK key object."""
        result = ChaCha20Poly1305Jwk()
        self.assertEqual(result.kty, 'oct')
        self.assertEqual(result.alg, 'C20P')
        self.assertEqual(result.use, 'enc')
        self.assertNotIn('k', result._data)
        self.assertNotIn('kid', result._data)

    def test_constructor_with_json(self):
        """Make an ChaCha20/Poly1305 JWK key from JSON data."""
        result = ChaCha20Poly1305Jwk(from_json=JWK_CHACHA20POLY1305)
        self.assertEqual(result.kty, 'oct')
        self.assertEqual(result.alg, 'C20P')
        self.assertEqual(result.use, 'enc')
        self.assertEqual(result.k, JWK_CHACHA20POLY1305_DICT['k'])
        self.assertEqual(result.kid, JWK_CHACHA20POLY1305_DICT['kid'])

    def test_constructor_with_dict(self):
        """Make an ChaCha20/Poly1305 JWK key from data in dict."""
        result = ChaCha20Poly1305Jwk(
            from_dict=json.loads(JWK_CHACHA20POLY1305))
        self.assertEqual(result.kty, 'oct')
        self.assertEqual(result.alg, 'C20P')
        self.assertEqual(result.use, 'enc')
        self.assertEqual(result.k, JWK_CHACHA20POLY1305_DICT['k'])
        self.assertEqual(result.kid, JWK_CHACHA20POLY1305_DICT['kid'])

    def test_constructor_with_dict_binary_k(self):
        """Make an ChaCha20/Poly1305 JWK key from data in dict, binary k."""
        result = ChaCha20Poly1305Jwk(from_dict=JWK_CHACHA20POLY1305_DICT)
        self.assertEqual(result.kty, 'oct')
        self.assertEqual(result.alg, 'C20P')
        self.assertEqual(result.use, 'enc')
        self.assertEqual(result.k, JWK_CHACHA20POLY1305_DICT['k'])
        self.assertEqual(result.kid, JWK_CHACHA20POLY1305_DICT['kid'])

    def test_constructor_with_k(self):
        """Make an ChaCha20/Poly1305 JWK key from secret."""
        result = ChaCha20Poly1305Jwk(from_secret=utils.bytes_to_string(
            JWK_CHACHA20POLY1305_DICT['k']))
        self.assertEqual(result.kty, 'oct')
        self.assertEqual(result.alg, 'C20P')
        self.assertEqual(result.use, 'enc')
        self.assertEqual(result.k, JWK_CHACHA20POLY1305_DICT['k'])
        self.assertEqual(result.kid, None)

    def test_constructor_with_k_binary(self):
        """Make an ChaCha20/Poly1305 JWK key from binary secret."""
        result = ChaCha20Poly1305Jwk(
            from_secret=JWK_CHACHA20POLY1305_DICT['k'])
        self.assertEqual(result.kty, 'oct')
        self.assertEqual(result.alg, 'C20P')
        self.assertEqual(result.use, 'enc')
        self.assertEqual(result.k, JWK_CHACHA20POLY1305_DICT['k'])
        self.assertEqual(result.kid, None)

    @mock.patch('nacl.utils.random', autospec=True)
    def test_constructor_generate(self, random_mock):
        """Generate an ChaCha20/Poly1305 JWK key."""
        random_mock.return_value = JWK_CHACHA20POLY1305_DICT['k']
        result = ChaCha20Poly1305Jwk(generate=True)
        self.assertEqual(random_mock.call_count, 1)
        self.assertEqual(result.kty, 'oct')
        self.assertEqual(result.alg, 'C20P')
        self.assertEqual(result.use, 'enc')
        self.assertEqual(result.k, JWK_CHACHA20POLY1305_DICT['k'])
        self.assertNotIn('kid', result._data)

    def test_constructor_clash(self):
        """Calling constuctor with JSON and generate options."""
        self.assertRaises(ValueError, ChaCha20Poly1305Jwk,
                          from_json=JWK_CHACHA20POLY1305,
                          generate=True)

    def test_to_json_no_private(self):
        """Serialise ChaCha20/Poly1305 JWK to JSON without private elements."""
        key = ChaCha20Poly1305Jwk(from_json=JWK_CHACHA20POLY1305)
        result = key.to_json(exclude_private=True)
        result_dict = json.loads(result)
        compare = json.loads(JWK_CHACHA20POLY1305)
        del compare['k']
        self.assertDictEqual(result_dict, compare)

    def test_to_json_with_private(self):
        """Serialise an ChaCha20/Poly1305 JWK to JSON with private elements."""
        key = ChaCha20Poly1305Jwk(from_json=JWK_CHACHA20POLY1305)
        result = key.to_json()
        result_dict = json.loads(result)
        compare = json.loads(JWK_CHACHA20POLY1305)
        self.assertDictEqual(result_dict, compare)

    def test_json_property(self):
        """Serialise an ChaCha20/Poly1305 JWK to JSON via JSON property."""
        key = ChaCha20Poly1305Jwk(from_json=JWK_CHACHA20POLY1305)
        result = key.json
        compare = key.to_json()
        self.assertEqual(result, compare)

    def test_setting_key_property_as_bytes(self):
        """Setting key property with bytes."""
        key_value = b'pork' * 8
        key = ChaCha20Poly1305Jwk()
        key.k = key_value
        self.assertEqual(key.k, key_value)

    def test_setting_key_property_as_str(self):
        """Setting key property with a str."""
        key_value = 'cG9ya3Bvcmtwb3JrcG9ya3Bvcmtwb3JrcG9ya3Bvcms'
        key = ChaCha20Poly1305Jwk()
        key.k = key_value
        self.assertEqual(key.k, 8 * b'pork')

    def test_setting_key_property_bad_key_size(self):
        """Setting key property with a mis-matched key size."""
        key = ChaCha20Poly1305Jwk()
        with self.assertRaises(ValueError):
            key.k = b"Don't Panic!"

    def test_setting_key_property_bad_key_type(self):
        """Setting key property with a mis-matched key type."""
        key = ChaCha20Poly1305Jwk()
        with self.assertRaises(ValueError):
            key.k = 42


if __name__ == '__main__':
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
